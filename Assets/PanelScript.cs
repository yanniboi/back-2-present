using UnityEngine;

public class PanelScript : MonoBehaviour
{
    public GameObject startMenu;
    public GameObject winMenu;
    
    void OnEnable()
    {
        if (PlayerController.gameComplete)
        {
            winMenu.SetActive(true);
            startMenu.SetActive(false);
        }
        else
        {
            startMenu.SetActive(true);
            winMenu.SetActive(false);
        }
    }

}

%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PlayerHands
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 0
  - m_Path: Armature/Root/Body
    m_Weight: 0
  - m_Path: Armature/Root/Body/Hand.L
    m_Weight: 1
  - m_Path: Armature/Root/Body/Hand.R
    m_Weight: 1
  - m_Path: Armature/Root/Body/Head
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R
    m_Weight: 0
  - m_Path: Armature/Root/Look_IK
    m_Weight: 0
  - m_Path: ObjObject
    m_Weight: 0

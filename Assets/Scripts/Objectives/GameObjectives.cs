using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjects.GameEvents.Events;
using ScriptableObjects.GameEvents.Listeners;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;


public class GameObjectives : MonoBehaviour
{
    private Objective[] objectives;
    public Text ObjectiveText;
    
    public void OnDoorOpen()
    {
        CompleteItem(ObjectiveStates.OpenDoor);
    }

    public void OnBatteryFound()
    {
        CompleteItem(ObjectiveStates.FindBattery);
    }

    private void CompleteItem(ObjectiveStates state)
    {
        var item = objectives
            .Where(x => !x.Complete)
            .FirstOrDefault(x => x.State == state);
        if (item == null)
        {
            return;
        }

        item.Complete = true;
        var newItem = objectives
            .FirstOrDefault(x => !x.Complete);
        if (newItem != null)
        {
            ObjectiveText.text = newItem.Text;
        }
        else
        {
            ObjectiveText.text = string.Empty;
        }
    }
    
    public void OnCharge()
    {
        CompleteItem(ObjectiveStates.ChargeTimeMachine);
    }
    
    public void OnTimeTravel()
    {
        CompleteItem(ObjectiveStates.EnterTimeMachine);
    }
    
    public void FoundKey()
    {
        CompleteItem(ObjectiveStates.FindKey);
    }
    
    public void OpenDoorTerminal()
    {
        CompleteItem(ObjectiveStates.OpenDoorTerminal);
    }
    
    void Start()
    {
        objectives = new []
        {
            new Objective {Text = "Open door", State = ObjectiveStates.OpenDoor},
            new Objective {Text = "Charge the time machine (1/3)", State = ObjectiveStates.ChargeTimeMachine},
            new Objective {Text = "Enter time machine", State = ObjectiveStates.EnterTimeMachine},
            new Objective {Text = "Find key card", State = ObjectiveStates.FindKey},
            new Objective {Text = "Open door with terminal", State = ObjectiveStates.OpenDoorTerminal},
            new Objective {Text = "Charge the time machine (2/3)", State = ObjectiveStates.ChargeTimeMachine},
            new Objective {Text = "Charge the time machine (3/3)", State = ObjectiveStates.ChargeTimeMachine},
            new Objective {Text = "Travel home", State = ObjectiveStates.TravelHome}
        };
        ObjectiveText.text = objectives.First().Text;
    }

    private class Objective
    {
        public string Text { get; set; }
        public ObjectiveStates State { get; set; }
        public bool Complete { get; set; }
    }

    private enum ObjectiveStates
    {
        OpenDoor,
        FindBattery,
        ChargeTimeMachine,
        EnterTimeMachine,
        FindKey,
        OpenDoorTerminal,
        TravelHome
    }
}


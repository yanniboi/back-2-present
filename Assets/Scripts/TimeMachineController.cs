﻿using System.Linq;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class TimeMachineController : MonoBehaviour
{
    private int batteryLevel;
    public GameObject travelArea;
    public VoidEvent charge;
    public IntEvent batteryCharged;
    private TriggerController chargeTrigger;

    private void Start()
    {
        batteryCharged.Raise(batteryLevel);
        chargeTrigger = GetComponentInChildren<TriggerController>();
    }

    public void Charge()
    {
        batteryLevel++;
        batteryCharged.Raise(batteryLevel);
        travelArea.SetActive(true);
        foreach (var chargeTriggerGameObject in chargeTrigger.gameObjects.ToList())
        {
            chargeTrigger.gameObjects.Remove(chargeTriggerGameObject);
            Destroy(chargeTriggerGameObject);
        }
    }

    public void BatteryPlaced()
    {
        charge.Raise();
    }

}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static bool gameComplete;
    //Movement
    [Header("Movement")]
    public Transform cam;

    public float speed = 1;
    private float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private Vector3 moveVec;
    private Rigidbody body;

    private bool canDrop;
    private bool batteryCharged;
    public GameObjectEvent dropEvent;
    private Rigidbody pickupBody;
    private Quaternion currentDirection;

    //Animation
    private Animator anim;

    private bool hasKey;
    
    //Time travel
    private TimeHopController timeHopController;

    public VoidEvent openSecondDoor;
    public VoidEvent foundKey;
    
    // Start is called before the first frame update
    void Start()
    {
        body = gameObject.GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        timeHopController = GetComponent<TimeHopController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        /*****************
         * Player Movement
         * ***************
         */

        //TODO: Add Gravity

        //If Player Input is Active
        if (moveVec.magnitude >= 0.1f)
        {
            //rotating towards move direction
            float targetAngle = Mathf.Atan2(moveVec.x, moveVec.z) * Mathf.Rad2Deg + cam.eulerAngles.y; //convert movement vector to angle, and add camera forward
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime); //dampen between current angle and desired angle
            transform.rotation = Quaternion.Euler(0f, angle, 0f); //move the player to face that direction

            anim.SetBool("Running", true); //set running bool for animations

            currentDirection = Quaternion.Euler(0f, targetAngle, 0f);
            Vector3 moveDir = currentDirection * Vector3.forward; //convert it back into a vector3
            body.MovePosition(body.position + moveDir * speed * Time.fixedDeltaTime); //move the player in that direction
        }
        else
        {
            anim.SetBool("Running", false); //if there is no input, obviously no running animation
        }
        
        if (pickupBody != default)
        {
            UpdatePickupPosition();
        }
        
    }

    public void SetJoint(GameObject o)
    {
        if (o.CompareTag("Key"))
        {
            PickupKey(o);
            return;
        }
        if (o.CompareTag("Terminal"))
        {
            UseTerminal(o);
            return;
        }
        pickupBody = o.GetComponent<Rigidbody>();
        pickupBody.isKinematic = true;
        UpdatePickupPosition();
        ResizePickup(0.75f);
        anim.SetBool("HoldingObject", true);
        
        StartCoroutine(WaitToDrop());
    }

    private void UseTerminal(GameObject o)
    {
        if (!hasKey) return;
        openSecondDoor.Raise();
        
    }

    private void PickupKey(GameObject o)
    {
        Destroy(o);
        hasKey = true;
        foundKey.Raise();
    }

    IEnumerator WaitToDrop()
    {
        yield return new WaitForSeconds(1);
        canDrop = true;
        yield return null;
    }

    public void RemoveJoint()
    {
        anim.SetBool("HoldingObject", false);
        pickupBody.isKinematic = false;
        ResizePickup(1f);
        pickupBody.position = body.position + (currentDirection * new Vector3(0, 1.2f, 1.6f));
        pickupBody = null;
    }
    
    public void OnMove(InputValue input)
    {
        Vector2 inputVec = input.Get<Vector2>();
        moveVec = new Vector3(inputVec.x, 0f, inputVec.y);
    }
    
    public void OnInteract()
    {
        if (canDrop && pickupBody != default)
        {
            dropEvent.Raise(pickupBody.gameObject);
            canDrop = false;
        }
    }

    public void Charge(int level)
    {
        batteryCharged = level >= 3;
    }

    public void TimeTravel()
    {
        if (canDrop) return;
        if (batteryCharged)
        {
            gameComplete = true;
            SceneManager.LoadScene("TitleMenu");
        }
        timeHopController.OnTimeHop();
    }

    public void StartGame()
    {
        gameComplete = false;
        SceneManager.LoadScene("Main");
    }

    private void UpdatePickupPosition()
    {
        pickupBody.position = body.position + (currentDirection * new Vector3(0, 3f, 0f));
    }

    private void ResizePickup(float scaleMultiplier)
    {
        pickupBody.transform.localScale = new Vector3(1, 1, 1) * scaleMultiplier;
    }

}

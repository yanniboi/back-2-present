﻿using System.Collections;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class TimeHopController : MonoBehaviour
{

    //[SerializeField] private Camera camera;
    [SerializeField] private Animator panelAnimator;
    [SerializeField] private GameObject timeMachine;
    [SerializeField] private Animator cameraAnimator;
    [SerializeField] private IntEvent yearEvent;
    
    private bool _isPast = false;
    private static readonly int Flash = Animator.StringToHash("Flash");
    public float timeHopMagnitude = 500;

    public void OnTimeHop()
    {
        if (_isPast)
        {
            MoveToPresent();
        }
        else
        {
            MoveToPast();
        }
        
        _isPast = !_isPast;
        
    }

    private void MoveToPast()
    {
        panelAnimator.SetTrigger(Flash);
        StartCoroutine(MoveCamera(Vector3.down * timeHopMagnitude));
    }
    private void MoveToPresent()
    {
        panelAnimator.SetTrigger(Flash);
        StartCoroutine(MoveCamera(Vector3.up * timeHopMagnitude));
    }

    private IEnumerator MoveCamera(Vector3 cameraOffset)
    {
        yield return new WaitForSeconds(0.2f);
        // var updatedPosition = camera.transform.position = camera.transform.position + (cameraOffset);
        transform.position = transform.position + (cameraOffset);
        timeMachine.transform.position = timeMachine.transform.transform.position + (cameraOffset);
        int timeInt = _isPast ? 1 : 0; 
        cameraAnimator.SetInteger("Time", timeInt);
        yearEvent.Raise(timeInt);
    }
}

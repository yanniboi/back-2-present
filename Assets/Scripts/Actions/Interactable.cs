﻿using System;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public GameObjectEvent enterAction;
    public VoidEvent leaveAction;
    private Light interactionGlow;

    private void Start()
    {
        interactionGlow = GetComponentInChildren<Light>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("PickupArea"))
        {
            enterAction.Raise(gameObject);
            if (interactionGlow)
            {
                interactionGlow.intensity = 5000000;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("PickupArea"))
        {
            leaveAction.Raise();
            if (interactionGlow)
            {
                interactionGlow.intensity = 0;
            }
        }
    }
}
﻿using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class PickupArea : MonoBehaviour
{
    public GameObjectEvent pickupEvent;
    private GameObject targetObject;
    private bool canTravel;
    public VoidEvent timeTravel;
    public void AtTarget(GameObject o)
    {
        targetObject = o;
    }
    
    public void LeaveTarget()
    {
        targetObject = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("TimeTravelArea"))
        {
            canTravel = true;
            Light interactionGlow = other.gameObject.GetComponentInChildren<Light>();
            if (interactionGlow)
            {
                interactionGlow.intensity = 5000000;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("TimeTravelArea"))
        {
            canTravel = false;
            Light interactionGlow = other.gameObject.GetComponentInChildren<Light>();
            if (interactionGlow)
            {
                interactionGlow.intensity = 0;
            }
        }
    }
    
    public void OnInteract()
    {
        if (canTravel)
        {
            timeTravel.Raise();
        }
        
        if (targetObject != null)
        {
            pickupEvent.Raise(targetObject);
        }
    }
}
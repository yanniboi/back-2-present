using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class TriggerController : MonoBehaviour
{
    public VoidEvent enterAction;
    public VoidEvent leaveAction;

    public HashSet<GameObject> gameObjects = new HashSet<GameObject>();
    public void EnterAction(GameObject o)
    {
        
        gameObjects.Add(o);
        if (gameObjects.Count == 1)
        {
            enterAction.Raise();
        }
    }

    public void LeaveAction(GameObject o)
    {
        gameObjects.Remove(o);
        if (!gameObjects.Any())
        {
            leaveAction.Raise();
        }
    }
}

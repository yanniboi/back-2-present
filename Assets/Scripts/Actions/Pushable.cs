using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pushable : MonoBehaviour
{
    public string layer = "Action";
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(layer))
        {
            other.gameObject.GetComponent<TriggerController>().EnterAction(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(layer))
        {
            other.gameObject.GetComponent<TriggerController>().LeaveAction(gameObject);
        }
    }
}
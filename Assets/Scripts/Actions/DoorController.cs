using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float openY = 4f;

    public float closedY = 0.25f;

    public void Open()
    {
        var pos = transform.position;
        transform.position = new Vector3(pos.x, openY, pos.z);
    }
    public void Close()
    {
        var pos = transform.position;
        transform.position = new Vector3(pos.x, closedY, pos.z);
    }
}

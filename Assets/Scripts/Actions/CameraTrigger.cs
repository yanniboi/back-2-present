using UnityEngine;

public class CameraTrigger : MonoBehaviour
{
    public Animator animatorController;
    public int roomId;
    // Start is called before the first frame update

    public void SetCamera(string room)
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("PickupArea"))
        {
            animatorController.SetInteger("Camera", roomId);
        }
    }
}

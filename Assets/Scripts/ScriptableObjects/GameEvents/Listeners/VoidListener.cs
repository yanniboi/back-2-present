﻿using ScriptableObjects.GameEvents.Events;

namespace ScriptableObjects.GameEvents.Listeners
{
    public class VoidListener : BaseGameEventListener<Void, VoidEvent, UnityVoidEvent>
    {
    
    }
}
﻿using ScriptableObjects.GameEvents.Events;
using UnityEngine;

namespace ScriptableObjects.GameEvents.Listeners
{
    public class GameObjectListener : BaseGameEventListener<GameObject, GameObjectEvent, UnityGameObjectEvent>
    {
    
    }
}
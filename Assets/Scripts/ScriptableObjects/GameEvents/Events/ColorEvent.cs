﻿using UnityEngine;

namespace ScriptableObjects.GameEvents.Events
{
    [CreateAssetMenu(fileName = "New Color Event", menuName="Game Events/Color Event")]
    public class ColorEvent : BaseGameEvent<Color> 
    {
    }
}
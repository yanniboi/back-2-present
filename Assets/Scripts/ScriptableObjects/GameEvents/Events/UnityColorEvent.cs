﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects.GameEvents.Events
{
    [Serializable]
    public class UnityColorEvent : UnityEvent<Color>
    {
    
    }
}
﻿using UnityEngine;

namespace ScriptableObjects.GameEvents.Events
{
    [CreateAssetMenu(fileName = "New Int Event", menuName="Game Events/Int Event")]
    public class IntEvent : BaseGameEvent<int> 
    {
    }
}
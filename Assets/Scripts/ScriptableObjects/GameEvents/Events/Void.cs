﻿using System;

namespace ScriptableObjects.GameEvents.Events
{
    [Serializable]
    public struct Void
    {
    }
}
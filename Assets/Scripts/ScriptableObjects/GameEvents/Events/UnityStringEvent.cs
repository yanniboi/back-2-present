﻿using System;
using UnityEngine.Events;

namespace ScriptableObjects.GameEvents.Events
{
    [Serializable]
    public class UnityStringEvent : UnityEvent<string>
    {
    
    }
}
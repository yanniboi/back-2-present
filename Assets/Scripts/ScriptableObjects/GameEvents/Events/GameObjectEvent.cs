﻿using UnityEngine;

namespace ScriptableObjects.GameEvents.Events
{
    [CreateAssetMenu(fileName = "New GameObject Event", menuName="Game Events/GameObject Event")]
    public class GameObjectEvent : BaseGameEvent<GameObject> 
    {
    }
}
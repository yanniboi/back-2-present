﻿using System;
using UnityEngine.Events;

namespace ScriptableObjects.GameEvents.Events
{
    [Serializable]
    public class UnityIntEvent : UnityEvent<int>
    {
    
    }
}
using System.Collections;
using System.Collections.Generic;
using System.Text;
using ScriptableObjects.GameEvents.Events;
using UnityEngine;

public class BatteryUI : MonoBehaviour
{
    public GameObject batteryCell0;
    public GameObject batteryCell1;
    public GameObject batteryCell2;
    
    public void OnBatteryCharge(int level)
    {
        var cells = level switch
        {
            1 => new {cell0 = true, cell1 = false, cell2 = false},
            2 => new {cell0 = true, cell1 = true, cell2 = false},
            3 => new {cell0 = true, cell1 = true, cell2 = true},
            _  => new {cell0 = false, cell1 = false, cell2 = false}
        };
       batteryCell0.SetActive(cells.cell0);     
       batteryCell1.SetActive(cells.cell1);     
       batteryCell2.SetActive(cells.cell2);     
    }
}

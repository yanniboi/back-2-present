using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private AudioSource audioSource;

    public AudioClip door;
    public float doorVolume = 1f;

    public AudioClip drop;
    public float dropVolume = 1f;

    public AudioClip terminal;
    public float terminalVolume = 1f;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void OpenDoor()
    {
        audioSource.pitch = 2f;
        audioSource.volume = doorVolume;
        audioSource.PlayOneShot(door);
    }

    public void DropBox()
    {
        audioSource.pitch = 1f;
        audioSource.volume = dropVolume;
        audioSource.PlayOneShot(drop);
    }

    public void UseTerminal()
    {
        audioSource.pitch = 1f;
        audioSource.volume = terminalVolume;
        audioSource.PlayOneShot(terminal);
    }
}
